import mock from './../mock';
import _ from '@lodash';

const eCommerceDB = {
    orders  : [
        {
            'id'                : '1',
            'namaPekerjaan'     : 'Penyusunan Draft Notulen Hasil Rapat / Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan',
            'status'            : true,
            'aksi'              : false
        },
        {
            'id'                : '2',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Dokumen / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Rekapitulasi dalam Suatu Periode',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan',
            'status'            : false,
            'aksi'              : true
        },
        {
            'id'                : '3',
            'namaPekerjaan'     : 'Penyusunan Draft Bahan / Materi Rapat Koordinasi',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Dokumen Bahan / Materi',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama',
            'status'            : false,
            'aksi'              : true
        },
        {
            'id'                : '4',
            'namaPekerjaan'     : 'Penyusunan Draft Notulen Hasil Rapat / Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '5',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '6',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         :  'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '7',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        },
        {
            'id'                : '8',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        },
        {
            'id'                : '9',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        },
        {
            'id'                : '10',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        },
        {
            'id'                : '11',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '12',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '13',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '14',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '15',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '16',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '17',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Tambahan'
        },
        {
            'id'                : '18',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        },
        {
            'id'                : '19',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        },
        {
            'id'                : '20',
            'namaPekerjaan'     : 'Pemberian Masukan, Koreksi, Paraf / Tanda Tangan oleh Atasan atas Penyusunan Laporan Hasil Rapat',
            'deskripsi'         : 'Sosialisasi PPDB Online jenjang SPS, TPA, KB, RA, MI, SD, SMP dan MTs se kabupaten Gunungkidul Tapel 2019/2020',
            'satuan'            : 'Buah / Dokumen',
            'buktiPekerjaan'    : 'Draft / Arsip Laporan Hasil Rapat',
            'keterangan'        : 'Setiap Dokumen',
            'metode'            : 'Satu Per Satu Pekerjaan',
            'tanggal'           : '22 Juni 2019',
            'kategori'          : 'Utama'
        }
    ]
};

mock.onGet('/api/e-commerce-app/orders').reply(() => {
    return [200, eCommerceDB.orders];
});

mock.onGet('/api/buku').reply(() => {
    return [200, eCommerceDB.buku];
});

mock.onGet('/api/e-commerce-app/order').reply((request) => {
    const {orderId} = request.params;
    const response = _.find(eCommerceDB.orders, {'id': orderId});
    return [200, response];
});

mock.onGet('/api/e-commerce-app/bulan').reply((request) => {
    const { kategori } = request.params;
    const response = _.find(eCommerceDB.orders, { 'kategori': kategori });
    return [200, response];
});