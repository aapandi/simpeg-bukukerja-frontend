/**
 * Authorization Roles
 */
const authRoles = {
    admin    : ['admin'],
    staff    : ['admin', 'staff'],
    user     : ['admin', 'staff', 'user','pejabat','ptk'],
    onlyGuest: []
};

export default authRoles;
