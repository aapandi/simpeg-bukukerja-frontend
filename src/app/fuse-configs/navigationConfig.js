const navigationConfig = [
    {
        'id'   : 'beranda-component',
        'title': 'Beranda',
        'type' : 'item',
        'icon' : 'dashboard',
        'url'  : '/buku-kerja/beranda'
    },
    {
        'id'   : 'buku-component',
        'title': 'Buku Kerja',
        'type' : 'item',
        'icon' : 'library_books',
        'url': '/buku-kerja/buku'
    },
    {
        'id'   : 'verifikasi-component',
        'title': 'Verifikasi Bawahan',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/buku-kerja/verifikasi-bawahan'
    },
    {
        'id'   : 'kinerja-component',
        'title': 'Perhitungan Kinerja',
        'type' : 'item',
        'icon' : 'insert_chart',
        'url'  : '/buku-kerja/perhitungan-kinerja'
    }
];

export default navigationConfig;
