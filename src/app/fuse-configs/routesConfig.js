import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse';
import {BerandaConfig} from 'app/main/beranda/BerandaConfig';
import {BukuKerjaConfig} from 'app/main/bukuKerja/BukuKerjaConfig';
import {VerifikasiBawahanConfig} from 'app/main/verifikasiBawahan/VerifikasiBawahanConfig';
import {PerhitunganKinerjaConfig} from 'app/main/perhitunganKinerja/PerhitunganKinerjaConfig';
import {LoginConfig} from 'app/main/login/LoginConfig';

const routeConfigs = [
    BerandaConfig,
    BukuKerjaConfig,
    VerifikasiBawahanConfig,
    PerhitunganKinerjaConfig,
    LoginConfig
];

const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs),
    {
        path     : '/',
        component: () => <Redirect to="/buku-kerja/beranda"/>
    }
];

export default routes;
