import React,{useEffect, useState} from 'react';
import { makeStyles, Grid, Paper, Typography, Breadcrumbs, Link, LinearProgress, Avatar} from '@material-ui/core';
import {FusePageSimple} from '@fuse';
import withReducer from 'app/store/withReducer';
import reducer from './store/reducers';
import classNames from 'classnames';
import EventIcon from '@material-ui/icons/Event';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import EventBusyIcon from '@material-ui/icons/EventBusy';
import DataTab from './DataTab';
import { useSelector, useDispatch } from "react-redux";
import * as Actions from './store/actions'

const useStyles = makeStyles(theme => ({
    layoutRoot: {},
    bgHeader: {
        background: '#2c9eff'
    },
    link: {
        display: 'flex'
    },
    linkBreadcrumb: {
        color: '#c3ebff !important'
    },
    paper: {
        padding: 12,
        color: theme.palette.text.secondary
    },
    eventIcon: {
        fontSize: 32
    },
    event1: {
        display: 'inline-block',
        backgroundColor: '#fb9678',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #fb9678',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #fb9678bd'
    },
    bgEvent1: {
        '& > div': {
            backgroundColor: '#fb9678'
        }
    },
    event2: {
        display: 'inline-block',
        backgroundColor: '#01c0c8',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #01c0c8',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #01c0c8bd'
    },
    bgEvent2: {
        '& > div': {
            backgroundColor: '#01c0c8'
        }
    },
    event3: {
        display: 'inline-block',
        backgroundColor: '#e46a76',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #e46a76',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #e46a76bd'
    },
    bgEvent3: {
        '& > div': {
            backgroundColor: '#e46a76'
        }
    },
    headline: {
        width: '100%',
        backgroundImage: 'radial-gradient(circle farthest-corner at 6.4% 9.8%, rgba(88,242,174,1) 0%, rgba(0,130,240,1) 97.9%)',
        backgroundSize: 'cover',
        borderRadius: 4
    },
    bigAvatar: {
        margin: '0 auto 12px',
        width: 150,
        height: 150,
        boxShadow: '0px 10px 30px #00000047'
    },
    sycnBox: {
        display: 'block',
        padding: '24px',
        borderTop: '2px solid #b6e9ff',
        borderBottom: '2px solid #b6e9ff',
        backgroundColor: '#cdeefd',
        borderRadius: '4px',
        boxShadow: '0 10px 10px -15px #004a6c',
        textAlign: 'justify',
        '& p': {
            fontWeight: 700
        }
    }
}));



function Beranda() {
  
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector(({ auth }) => auth.user);
    const pegawai = useSelector(({beranda}) => beranda.beranda.data);

    useEffect(() => {
        if(pegawai["@id"].length<2){
            // dispatch(Actions.getPegawai());
        }
    }, []);

    return (
        <FusePageSimple
            classes={{
                root: classes.layoutRoot,
                header: classNames(classes.bgHeader, "min-h-128 h-128 sm:min-h-52 sm:h-52"),
                content: "p-12"
            }}
            header={
                <div className="flex flex-1 items-center justify-between p-12">
                    <div>
                        <Typography variant="subtitle1">Selamat Datang, <strong>{user.data.displayName}</strong></Typography>
                    </div>
                    <div>
                        <Breadcrumbs separator="›" aria-label="Breadcrumb">
                            <Link href="/" className={classes.linkBreadcrumb}>
                                Beranda
                                </Link>
                            <Typography color="textPrimary">Dasbor</Typography>
                        </Breadcrumbs>
                    </div>
                </div>
            }
            content={
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Typography variant="h6" color="textPrimary" gutterBottom>Versi Rilis</Typography>
                            <Typography variant="body1">Buku Kerja Non ASN Di Lingkungan Pemerintah Dinas Pendidikan Kabupaten Gunungkidul.</Typography>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper className={classes.paper}>
                            <div className="flex flex-1 items-center justify-between mb-12">
                                <div>
                                    <EventIcon className={classes.eventIcon} />
                                    <Typography variant="subtitle2" color="textPrimary">PERIODE, <span style={{ color: "#e46a76", fontWeight: 700 }}>2019-06-01 s/d 2019-06-30</span></Typography>
                                </div>
                                <div>
                                    <Typography variant="h5" className={classNames(classes.event1, classes.bgEvent1)}><strong>28</strong></Typography>
                                </div>
                            </div>
                            <LinearProgress color="primary" className={classes.bgEvent1} variant="determinate" value={80} />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper className={classes.paper}>
                            <div className="flex flex-1 items-center justify-between mb-12">
                                <div>
                                    <EventAvailableIcon className={classes.eventIcon} />
                                    <Typography variant="subtitle2" color="textPrimary"><span style={{ fontWeight: 700 }}>DISETUJUI</span></Typography>
                                </div>
                                <div>
                                    <Typography variant="h5" className={classNames(classes.event2, classes.bgEvent2)}><strong>26</strong></Typography>
                                </div>
                            </div>
                            <LinearProgress color="primary" className={classes.bgEvent2} variant="determinate" value={75} />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper className={classes.paper}>
                            <div className="flex flex-1 items-center justify-between mb-12">
                                <div>
                                    <EventBusyIcon className={classes.eventIcon} />
                                    <Typography variant="subtitle2" color="textPrimary"><span style={{ fontWeight: 700 }}>DITOLAK</span></Typography>
                                </div>
                                <div>
                                    <Typography variant="h5" className={classNames(classes.event3, classes.bgEvent3)}><strong>0</strong></Typography>
                                </div>
                            </div>
                            <LinearProgress color="primary" className={classes.bgEvent3} variant="determinate" value={0} />
                        </Paper>
                    </Grid>
                    <Grid item xs={6} sm={4}>
                        <Paper className={classNames(classes.paper, "p-0")}>
                            <div className={classNames(classes.headline, "text-center p-24")}>
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className={classes.bigAvatar} alt="Sudarno" />
                                <Typography variant="h6" style={{ color: '#ffffff' }}>{user.data.displayName}</Typography>
                                <Typography variant="subtitle2" style={{ color: '#f4f4f4' }}>-</Typography>
                            </div>

                            <div className="p-12">
                                <div className="mb-12">
                                    <Typography variant="caption" color="textSecondary" gutterBottom><strong>Jabatan</strong></Typography>
                                    <Typography component="p" color="primary" align="justify">{pegawai.jabatan}.</Typography>
                                </div>
                                <div className="mb-12">
                                    <Typography variant="caption" color="textSecondary" gutterBottom><strong>Unit Kerja / OPD</strong></Typography>
                                    <Typography component="p" color="primary" align="justify">{pegawai.tempatTugas}.</Typography>
                                </div>
                                {/* <div className={classes.sycnBox}>
                                    <Typography variant="body1" color="primary">Sinkronisasi Data terakhir pada 01-05-2019 Apabila tidak sesuai silahkan hubungi BKPPD Gunungkidul</Typography>
                                </div> */}
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item xs={6} sm={8}>
                        <Paper className={classes.paper}>
                            <DataTab />
                        </Paper>
                    </Grid>
                </Grid>
            }
        />
    )
}

export default withReducer('beranda', reducer)(Beranda);
