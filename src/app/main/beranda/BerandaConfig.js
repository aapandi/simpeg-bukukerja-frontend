import React from 'react';
import {authRoles} from "../../auth";

export const BerandaConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth:authRoles.user,
    routes  : [
        {
            path     : '/buku-kerja/beranda',
            component: React.lazy(() => import('./Beranda'))
        }
    ]
};