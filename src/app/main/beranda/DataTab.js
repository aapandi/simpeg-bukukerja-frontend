import React from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import DataAtasan from './dataTab/DataAtasan';
import RiwayatBukuKerja from './dataTab/RiwayatBukuKerja';

const AntTabs = withStyles({
    root: {
        borderBottom: '1px solid #e8e8e8'
    },
    indicator: {
        backgroundColor: 'rgba(0,130,240,1)'
    }
})(Tabs);

const AntTab = withStyles(theme => ({
    root: {
        textTransform: 'none',
        minWidth: 72,
        fontWeight: theme.typography.fontWeightRegular,
        marginRight: 12,
        fontSize: '1.4rem',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            color: '#40a9ff',
            opacity: 1,
        },
        '&$selected': {
            color: 'rgba(0,130,240,1)',
            fontWeight: theme.typography.fontWeightMedium,
        },
        '&:focus': {
            color: '#40a9ff'
        }
    },
    selected: {},
}))(props => <Tab disableRipple {...props} />);

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    typography: {
        padding: theme.spacing(2)
    },
    tabStyles: {
        backgroundColor: theme.palette.background.paper
    }
}));

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 6 * 2 }}>
            {props.children}
        </Typography>
    );
}

export default function DataAtasanRiwayat() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    function handleChange(event, newValue) {
        setValue(newValue);
    }

    return (
        <div className={classes.root}>
            <div className={classes.tabStyles}>
                <AntTabs value={value} onChange={handleChange}>
                    <AntTab label="Data Atasan" />
                    <AntTab label="Riwayat Buku Kerja" />
                </AntTabs>
                {value === 0 && <TabContainer><DataAtasan /></TabContainer>}
                {value === 1 && <TabContainer><RiwayatBukuKerja /></TabContainer>}
            </div>
        </div>
    );
}