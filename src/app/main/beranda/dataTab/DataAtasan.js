import React from 'react';
import {makeStyles, Grid, Typography, Divider} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    infoAtasan: {
        display: 'block',
        padding: 12,
        borderTop: '2px solid #f9f9f9',
        boxShadow: '0 10px 10px -15px #101010b8',
        textAlign: 'center',
        borderBottom: '2px solid #f9f9f9',
        borderRadius: 4,
        backgroundColor: '#ffffff',
        '& p': {
            fontWeight: 700
        }
    }
}));

export default function DataAtasan() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <div className="mb-12">
                        <Typography variant="caption" color="textPrimary" gutterBottom><strong>Nama Lengkap</strong></Typography>
                        <Typography variant="subtitle1">-</Typography>
                    </div>
                    <div className="mb-12">
                        <Typography variant="caption" color="textPrimary" gutterBottom><strong>NIP (Nomor Induk Pegawai)</strong></Typography>
                        <Typography variant="subtitle1">-</Typography>
                    </div>
                    <Divider variant="middle" className="mx-0 mb-12" />
                    <div>
                        <Typography variant="subtitle1" color="secondary"><strong>Atasan</strong> (Pemangku Jabatan)</Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <div className="mb-12">
                        <Typography variant="caption" color="textPrimary" gutterBottom><strong>Keterangan</strong></Typography>
                        <Typography variant="subtitle1" align="justify">-</Typography>
                    </div>
                </Grid>
                <Grid item xs={12}> 
                    <div className={classes.infoAtasan}>
                        <Typography variant="body1" color="primary">Apabila data tidak sesuai silahkan hubungi BKPPD Kabupaten Gunungkidul.</Typography>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}