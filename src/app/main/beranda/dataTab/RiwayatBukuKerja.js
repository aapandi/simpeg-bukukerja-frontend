import React from 'react';
import {makeStyles, Grid, Typography, Avatar, Button} from '@material-ui/core';
import MoodIcon from '@material-ui/icons/Mood';
import MoodBadIcon from '@material-ui/icons/MoodBad';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    infoAtasan: {
        display: 'block',
        padding: 12,
        borderTop: '2px solid #f9f9f9',
        boxShadow: '0 10px 10px -15px #101010b8',
        textAlign: 'center',
        borderBottom: '2px solid #f9f9f9',
        borderRadius: 4,
        backgroundColor: '#ffffff',
        '& p': {
            fontWeight: 700
        }
    },
    profileTimeline: {
        position: 'relative',
        paddingLeft: 40,
        marginRight: 10,
        borderLeft: '1px solid #e9ecef',
        marginLeft: 30,
        '& .sl__item': {
            marginTop: 8,
            marginBottom: 30,
            '& .sl__left': {
                float: 'left',
                marginLeft: -60,
                zIndex: 1,
                marginRight: 15,
                '& .pAvatar': {
                    width: 40,
                    height: 40
                }
            },
            '& .sl__right': {
                display: 'block',
                borderBottom: '1px solid #eee',
                paddingBottom: 24,
                '& .pMetaTop': {
                    marginBottom: 12,
                    '& .sl__date': {
                        display: 'inline-block',
                        backgroundColor: '#2c9eff',
                        padding: '.5rem 1.5rem',
                        borderRadius: '30px',
                        fontSize: '13px',
                        color: '#ffffff',
                        boxShadow: '0px 10px 20px -10px #2c9eff',
                    }
                },
                '& .pContent': {
                    marginBottom: 12
                },
                '& .pMetaBottom': {
                    marginTop: 32,
                    '& .pDate': {
                        padding: 0,
                        backgroundColor: 'transparent',
                        display: 'inline-block',
                        fontWeight: 700
                    },
                    '& .pStatus': {
                        padding: 0,
                        backgroundColor: 'transparent',
                        display: 'flex',
                        fontSize: '1.4rem',
                        textTransform: 'capitalize',
                        alignItems: 'center',
                        '&.textSuccess': {
                            color: '#00c292'
                        },
                        '&.textWarning': {
                            color: '#fec107'
                        }
                    }
                }
            }
        }
    },
    linkBtn: {
        padding: 0,
        textTransform: 'capitalize',
        backgroundColor: 'transparent !important',
        border: 'none',
        boxShadow: 'none',
        marginTop: -3,
        color: '#2c9eff'
    }
}));

export default function RiwayatBukuKerja() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <div className={classes.profileTimeline}>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">yesterday</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Membuat / mengembangkan sebuah program / aplikasi / sistem informasi (SIM).</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-22 13:52:57</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textWarning"><MoodBadIcon className="mr-4" /> Belum Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">yesterday</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menjadi narasumber dalam sebuah kegiatan.</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-22 13:52:12</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textWarning"><MoodBadIcon className="mr-4" /> Belum Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">2 days ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menjadi narasumber dalam sebuah kegiatan.</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-21 14:33:51</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">2 days ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menjadi narasumber dalam sebuah kegiatan.</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-21 14:32:47</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">2 days ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menjadi narasumber dalam sebuah kegiatan.</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-21 14:32:07</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">a week ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menyusun draft proposal kegiatan Dana Alokasi Khusus (DAK), Dana Dekonsentrasi (Dekon) dan Tugas Pembantuan (TP).</strong></Typography>
                                        <Typography component="p">Kategori Tugas Utama</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-16 00:09:18</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">a week ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menyusun draft proposal kegiatan Dana Alokasi Khusus (DAK), Dana Dekonsentrasi (Dekon) dan Tugas Pembantuan (TP).</strong></Typography>
                                        <Typography component="p">Kategori Tugas Utama</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-16 00:08:49</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">a week ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Membuat / mengembangkan sebuah program / aplikasi / sistem informasi (SIM).</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-16 00:08:05</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right">
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">a week ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Menyusun draft proposal kegiatan Dana Alokasi Khusus (DAK), Dana Dekonsentrasi (Dekon) dan Tugas Pembantuan (TP).</strong></Typography>
                                        <Typography component="p">Kategori Tugas Utama</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-14 18:45:52</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sl__item">
                            <div className="sl__left">
                                <Avatar src="https://images.unsplash.com/photo-1509021436665-8f07dbf5bf1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" className="pAvatar" alt="Sudarno" />
                            </div>
                            <div className="sl__right" style={{borderBottom: '0 solid #eee', paddingBottom: 0}}>
                                <div>
                                    <div className="pMetaTop">
                                        <div className="flex flex-1 justify-between items-center">
                                            <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>SUDARNO</Button> <span className="sl__date">a week ago</span>
                                        </div>
                                        <Typography component="p">Entri data ke dalam <Button href="/" color="inherit" disableRipple className={classes.linkBtn}>Buku Kerja</Button></Typography>
                                    </div>
                                    <div className="pContent">
                                        <Typography variant="subtitle1" color="primary" gutterBottom><strong>Membuat / mengembangkan sebuah program / aplikasi / sistem informasi (SIM).</strong></Typography>
                                        <Typography component="p">Kategori Tugas Tambahan</Typography>
                                    </div>
                                    <div className="pMetaBottom flex flex-1 justify-between">
                                        <Button href="/" color="inherit" disableRipple className="pDate">2019-06-14 18:41:28</Button>
                                        <Button href="/" color="inherit" disableRipple className="pStatus textSuccess"><MoodIcon className="mr-4" /> Disetujui</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}