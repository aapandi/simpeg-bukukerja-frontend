import axios from 'axios';

export const GET_PEGAWAI = '[E-COMMERCE APP] GET PEGAWAI';
export const GET_ORDERS = '[E-COMMERCE APP] GET ORDERS';
export const SET_ORDERS_SEARCH_TEXT = '[E-COMMERCE APP] SET ORDERS SEARCH TEXT';

export function getPegawai()
{
    console.log('pegawai');
    const request = axios.get('/api/pegawais');

    return (dispatch) =>
        request.then((response) => dispatch({
                type   : GET_PEGAWAI,
                payload: response.data['hydra:member'][0]
            })
        );
}

export function setOrdersSearchText(event)
{
    return {
        type      : SET_ORDERS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

