import * as Actions from '../actions';

const initialState = {
    data      : {
        "@id":"",
        "@type":"",
        jabatan: "",
        jenisKelamin: "",
        jenjangPendidikan: "",
        karpeg: "",
        lastUpdate: "",
        nama: "",
        nik: null,
        nip: "",
        npwp: null,
        pangkatGolongan: {},
        pegawaiId: "",
        softDelete: 0,
        statusKepegawaian: {},
        tempatLahir: "",
        tempatTugas: "",
        tglLahir: "",
        tmtPangkat: "",
        tmtPengangkatan: null
    },
    searchText: 'abc'
};

const berandaReducer = function (state = initialState, action) {
   
    switch ( action.type )
    {
        case Actions.GET_PEGAWAI:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_ORDERS_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default berandaReducer;
