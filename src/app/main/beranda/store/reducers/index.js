import {combineReducers} from 'redux';
import beranda from './beranda.reducer';

const reducer = combineReducers({
    beranda
});

export default reducer;
