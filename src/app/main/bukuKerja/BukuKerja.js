import React from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import {Grid, Paper, Typography, Breadcrumbs, FormControl, InputLabel, InputBase, NativeSelect, Button, Fab, Divider} from '@material-ui/core';
import {Link} from "react-router-dom";
import {FusePageSimple} from '@fuse';
import withReducer from 'app/store/withReducer';
import reducer from './store/reducers';
import classNames from 'classnames';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';
import BukuKerjaTable from './BukuKerjaTable';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import Formsy from 'formsy-react';

const BootstrapInput = withStyles(theme => ({
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        width: 'auto',
        minWidth: 180,
        padding: '8px 24px 8px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"'
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
        }
    }
}))(InputBase);

const useStyles = makeStyles(theme => ({
    layoutRoot: {},
    bgHeader: {
        background: '#2c9eff'
    },
    link: {
        display: 'flex'
    },
    linkBreadcrumb: {
        color: '#c3ebff !important'
    },
    paper: {
        padding: 12,
        color: theme.palette.text.secondary
    },
    eventIcon: {
        fontSize: 32
    },
    event1: {
        display: 'inline-block',
        backgroundColor: '#fb9678',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #fb9678',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #fb9678bd'
    },
    bgEvent1: {
        '& > div': {
            backgroundColor: '#fb9678'
        }
    },
    event2: {
        display: 'inline-block',
        backgroundColor: '#01c0c8',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #01c0c8',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #01c0c8bd'
    },
    bgEvent2: {
        '& > div': {
            backgroundColor: '#01c0c8'
        }
    },
    event3: {
        display: 'inline-block',
        backgroundColor: '#e46a76',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #e46a76',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #e46a76bd'
    },
    bgEvent3: {
        '& > div': {
            backgroundColor: '#e46a76'
        }
    },
    extendedButton: {
        margin: theme.spacing(1),
        padding: '0 16px !important'
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    dangerBtn: {
        backgroundColor: '#dc3f4e',
        '&:hover': {
            backgroundColor: '#cd3342'
        }
    }
}));

function BukuKerja() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const montInt = ((new Date()).getMonth()+1)
    const [bulan, setBulan] = React.useState(montInt);
    const handleChange = event => {
        setBulan(event.target.value);
    };
    // const orders = useSelector(({ bukuKerja }) => bukuKerja.orders.data);
    
    function clickFilterBulan() {
        console.log('click filter');
        console.log(bulan);
        // dispatch(Actions.filterByBulan('Utama'));
    }
    return (
        <FusePageSimple
            classes={{
                root        : classes.layoutRoot,
                header      : classNames(classes.bgHeader, "min-h-128 h-128 sm:min-h-52 sm:h-52"),
                content     : "p-12"
            }}
            header={
                <div className="flex flex-1 items-center justify-between p-12">
                    <div>
                        <Typography variant="subtitle1">Buku Kerja</Typography>
                    </div>
                    <div>
                        <Breadcrumbs separator="›" aria-label="Breadcrumb">
                            <Link to="/" className={classes.linkBreadcrumb}>
                                Beranda
                            </Link>
                            <Typography color="textPrimary">Buku Kerja</Typography>
                        </Breadcrumbs>
                    </div>
                </div>
            }
            content={
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className="w-full flex flex-1 items-center justify-between">
                                <Typography variant="subtitle2" color="textPrimary" className="mr-4">Lihat Buku Kerja Periode</Typography>
                                <div className="flex items-center">
                                    <FormControl className="mr-8">
                                        <InputLabel htmlFor="buku-kerja">Pilih Bulan</InputLabel>
                                        <NativeSelect
                                            value={bulan}
                                            onChange={handleChange}
                                            input={<BootstrapInput name="bulan" id="buku-kerja" />}
                                        >
                                            <option value=""></option>
                                            <option value={1}>Januari</option>
                                            <option value={2}>Februari</option>
                                            <option value={3}>Maret</option>
                                            <option value={4}>April</option>
                                            <option value={5}>Mei</option>
                                            <option value={6}>Juni</option>
                                            <option value={7}>Juli</option>
                                            <option value={8}>Agustus</option>
                                            <option value={9}>September</option>
                                            <option value={10}>Oktober</option>
                                            <option value={11}>November</option>
                                            <option value={12}>Desember</option>
                                        </NativeSelect>
                                    </FormControl>
                                    <Button onClick={clickFilterBulan} variant="contained" size="small" color="secondary" >
                                        Tampilkan
                                    </Button>
                                </div>
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className="w-full text-center p-24">
                                <Typography variant="h5" color="secondary" className="font-bold">PERIODE BUKU KERJA</Typography>
                                <Typography variant="subtitle2" color="textSecondary" gutterBottom>01 Juni 2019 s/d 30 Juni 2019</Typography>

                                <div>
                                    <Fab
                                        component={Link}
                                        to="/buku-kerja/buku/baru"
                                        variant="extended"
                                        size="small"
                                        color="secondary"
                                        aria-label="Add"
                                        className={classes.extendedButton}
                                    >
                                        <EditIcon className={classes.extendedIcon} />
                                        Isi Buku Kerja
                                    </Fab>
                                    <Fab
                                        component={Link}
                                        to="/buku-kerja/buku/cetak"
                                        variant="extended"
                                        size="small"
                                        color="inherit"
                                        aria-label="Add"
                                        className={classes.extendedButton}
                                    >
                                        <PrintIcon className={classes.extendedIcon} />
                                        Cetak Buku Kerja
                                    </Fab>
                                </div>
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Typography variant="subtitle1" color="primary" gutterBottom><strong>Riwayat Entry Buku Kerja Periode Ini</strong></Typography>
                            <Divider variant="middle" className="mx-0 mb-24" />
                            <BukuKerjaTable/>
                        </Paper>
                    </Grid>
                </Grid>   
            }
        />
    );
}

export default withReducer('bukuKerja', reducer)(BukuKerja);