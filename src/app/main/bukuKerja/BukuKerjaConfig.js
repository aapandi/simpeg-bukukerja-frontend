import React from 'react';
import {authRoles} from "../../auth";

export const BukuKerjaConfig = {
    settings: {
        layout: {}
    },
    auth:authRoles.user,
    routes  : [
        {
            path     : '/buku-kerja/buku/baru',
            component: React.lazy(() => import('./bukuKerjaBaru/BukuKerjaBaru'))
        },
        {
            path     : '/buku-kerja/buku/cetak',
            component: React.lazy(() => import('./bukuKerjaCetak/BukuKerjaCetak'))
        },
        {
            path    : '/buku-kerja/buku',
            component: React.lazy(() => import('./BukuKerja'))
        }
    ]
};