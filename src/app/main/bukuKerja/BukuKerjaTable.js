import React, {useEffect, useState} from 'react';
import {makeStyles, Table, TableHead, TableBody, TableCell, TablePagination, TableRow, Button, Typography} from '@material-ui/core';
import {FuseScrollbars, FuseUtils} from '@fuse';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import * as Actions from './store/actions';
import { green } from '@material-ui/core/colors';
import {useDispatch, useSelector} from 'react-redux';
import CancelIcon from '@material-ui/icons/Cancel';
import SuccesIcon from '@material-ui/icons/CheckCircle';
import MoodIcon from '@material-ui/icons/Mood';
import MoodBadIcon from '@material-ui/icons/MoodBad';
import clsx from 'clsx';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    batalBtn: {
      margin: theme.spacing(1),
      backgroundColor: '#dc3f4e',
      '&:hover': {
        backgroundColor: '#d93343'
      }
    },
    usulBtn: {
      margin: theme.spacing(1),
        backgroundColor: '#3ddb4b',
      '&:hover': {
          backgroundColor: '#33d946'
      }
    },
    leftIcon: {
      marginRight: theme.spacing(1)
    },
    rightIcon: {
      marginLeft: theme.spacing(1)
    },
    iconSmall: {
      fontSize: 16
    },
    textSuccess: {
        color: '#32CD32'
    },
    textPending: {
        color: '#fec107'
    },
    textDiajukan: {
        color: '#00c292'
    },
    textDibatalkan: {
        color: '#CD5C5C'
    },
    buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    fabProgress: {
        color: green[500],
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
}));

function BukuKerjaTable(props) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const bukuKerja = useSelector(({bukuKerja}) => bukuKerja.bukuKerja.data);
    const count = useSelector(({bukuKerja}) => bukuKerja.bukuKerja.count);
    const searchText = useSelector(({bukuKerja}) => bukuKerja.bukuKerja.searchText);
    const [selected] = useState([]);
    const dataPopup = useSelector(({bukuKerja})=> bukuKerja.bukuKerja.bukuKerjaSelected);
    const [data, setData] = useState(bukuKerja);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [order] = useState({
        direction: 'asc',
        id       : null
    });
    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const timer = React.useRef();

    const buttonClassname = clsx({
        [classes.buttonSuccess]: success,
    });

    function handleClickOpen(record) {
        // dataPopup = record;
        dispatch(Actions.setBukuKerjaSelected(record));
        setOpen(true);
        // console.log(dataPopup);
    }

    function handleClose() {
        setOpen(false);
    }

    useEffect(() => {
        dispatch(Actions.getBukuKerjas());
    }, [dispatch]);

    useEffect(() => {
        setData(searchText.length === 0 ? bukuKerja : FuseUtils.filterArrayByString(bukuKerja, searchText))
    }, [bukuKerja, searchText]);

    function handleChangePage(event, page) {
        setPage(page);
    }

    function handleChangeRowsPerPage(event) {
        setRowsPerPage(event.target.value);
    }

    function clickBatalkanUsulan(bukuKerjaId) {
        if (!loading) {
            setSuccess(false);
            setLoading(true);

            dispatch(Actions.batalkanUsulan(bukuKerjaId)).then(() => {
                setOpen(false);
                timer.current = setTimeout(() => {
                    setSuccess(true);
                    setLoading(false);
                }, 1000);

            });

        }
    }

    function clickUsulkanUlang(bukuKerjaId) {
            if (!loading) {
                setSuccess(false);
                setLoading(true);
        
                dispatch(Actions.usulUlang(bukuKerjaId)).then(()=>{
                    setOpen(false);
                    timer.current = setTimeout(() => {
                    setSuccess(true);
                    setLoading(false);
                    }, 1000);

                    });
        
            }
        }


    function formatDate(date) {
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let d = new Date(date),
            // month = '' + (d.getMonth() + 1),
            month = months[d.getMonth()] ,
            day = '' + d.getDate(),
            year = d.getFullYear();

        // if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join(' ');
    }

    return (
        <div className="w-full flex flex-col">
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Konfirmasi</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {dataPopup.statusVerifikasi === 1 ?
                            ("Setuju Batalkan Usulan ?"  ) 
                            : ("Setuju Lanjutkan Usul Ulang ?" ) 
                            }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Tidak
                </Button>
                    <div className={classes.wrapper}>
                        {dataPopup.statusVerifikasi === 4 ?
                        (<Button onClick={(e) => clickUsulkanUlang(dataPopup.bukuKerjaId)} color="primary" autoFocus 
                            variant="contained"
                            color="primary"
                            className={buttonClassname}
                            disabled={loading}
                        >Setuju ? </Button>)
                            : dataPopup.statusVerifikasi === 3 ? 
                            (<Button onClick={(e) =>clickUsulkanUlang(dataPopup.bukuKerjaId)} color="primary" autoFocus
                             variant="contained"
                            color="primary"
                            className={buttonClassname}
                            disabled={loading}
                            >Setuju  </Button>)
                            : (<Button onClick={(e) => clickBatalkanUsulan(dataPopup.bukuKerjaId)} color="primary" autoFocus
                             variant="contained"
                            color="primary"
                            className={buttonClassname}
                            disabled={loading}
                            >Setuju  </Button>)
                            }
                        {loading && <CircularProgress size={26} className={classes.buttonProgress} />}
                    </div>
                </DialogActions>
            </Dialog>

            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table aria-labelledby="tableTitle" size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" width="42">NO.</TableCell>
                            <TableCell align="center">TANGGAL PEKERJAAN</TableCell>
                            <TableCell>NAMA PEKERJAAN URAIAN</TableCell>
                            <TableCell align="center">KATEGORI</TableCell>
                            <TableCell align="center" width="120">KETERANGAN</TableCell>
                            <TableCell align="center" width="120">TANGGAL</TableCell>
                            <TableCell align="center" width="170">AKSI</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {
                            _.orderBy(data, [order.direction])
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((n,key) => {
                                    // console.log(key);
                                    const isSelected = selected.indexOf(n.id) !== -1;
                                    return (
                                        <TableRow
                                            className="h-64"
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            key={n.bukuKerjaId}
                                            selected={isSelected}
                                        >
                                            <TableCell component="th" scope="row" align="center">
                                                {(key+1)}
                                            </TableCell>

                                            <TableCell component="th" scope="row" align="center">
                                                {formatDate(n.tglPelaksanaan)} 
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                <Typography variant="subtitle1" className="font-bold" gutterBottom>{n.nama}</Typography>
                                                <Typography component="p">{n.keterangan}</Typography>
                                            </TableCell>

                                            <TableCell component="th" scope="row" align="center">
                                                {n.kategoriStr}
                                            </TableCell>

                                            <TableCell component="th" scope="row" align="center">
                                                {n.statusVerifikasi === 1 ?
                                                    (
                                                        <Typography className={clsx(classes.textDiajukan, "flex justify-center")}><MoodIcon className="mr-4" /> Diajukan</Typography>
                                                    ) :
                                                    n.statusVerifikasi === 2 ? (
                                                        <Typography className={clsx(classes.textSuccess, "flex justify-center")}><MoodIcon className="mr-4" /> Disetujui </Typography>
                                                    ) : 
                                                    n.statusVerifikasi === 3 ? (
                                                            <Typography className={clsx(classes.textDibatalkan, "flex justify-center")}><MoodBadIcon className="mr-4" /> Dibatalkan </Typography>
                                                    ) : (
                                                        <Typography className={clsx(classes.textPending, "flex justify-center")}><MoodIcon className="mr-4" /> Butuh Perbaikan </Typography>
                                                    )
                                                    
                                                }
                                            </TableCell>

                                            <TableCell component="th" scope="row" align="center">
                                                {/* {n.lastUpdateStatus} */}
                                                {formatDate(n.lastUpdateStatus)}
                                            </TableCell>

                                            <TableCell component="th" scope="row" align="center">
                                                {n.statusVerifikasi === 2 ?
                                                    (
                                                        "-"
                                                    ) : n.statusVerifikasi === 4 ?
                                                        (
                                                        // <Button onClick={(e) => { clickUsulkanUlang(n.bukuKerjaId) }} variant="contained" size="small" color="primary" className={classes.usulBtn}>
                                                            <Button onClick={(e) => { handleClickOpen(n) }} variant="contained" size="small" >
                                                                <SuccesIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                                                                Usul Ulang
                                                        </Button>
                                                        ) : n.statusVerifikasi === 3 ?
                                                            (
                                                                // <Button onClick={(e) => { clickUsulkanUlang(n.bukuKerjaId) }} variant="contained" size="small" color="primary" className={classes.usulBtn}>
                                                                <Button onClick={(e) => { handleClickOpen(n) }} variant="contained" size="small"  >
                                                                    <SuccesIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                                                                    Usul Ulang
                                                        </Button>
                                               ) : (
                                                        // <Button onClick={(e)=>{clickBatalkanUsulan(n.bukuKerjaId)}} variant="contained" size="small" color="primary" className={classes.batalBtn}>
                                                                <Button onClick={(e) => { handleClickOpen(n)}} variant="contained" size="small" >
                                                            <CancelIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                                                            Batal Usulan
                                                        </Button>
                                                    ) 
                                                    
                                                }
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                    </TableBody>
                </Table>
            </FuseScrollbars>

            <TablePagination
                component="div"
                count={count}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page'
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page'
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    );
}

export default withRouter(BukuKerjaTable);