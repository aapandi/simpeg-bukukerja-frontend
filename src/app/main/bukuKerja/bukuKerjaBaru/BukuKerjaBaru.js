import React, {useRef, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Paper, Typography, Breadcrumbs, Button, MenuItem, TextField} from '@material-ui/core';
import {Link} from "react-router-dom";
import {FusePageSimple, TextFieldFormsy, SelectFormsy} from '@fuse';
import Formsy from 'formsy-react';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';
import classNames from 'classnames';
import BukuKerjaBaruTable from './BukuKerjaBaruTable';
import * as Actions from '../store/actions';
import * as AppActions from 'app/store/actions';
import { useDispatch, useSelector } from 'react-redux';
import History from '../../../../@history';

const useStyles = makeStyles(theme => ({
    layoutRoot: {},
    bgHeader: {
        background: '#2c9eff'
    },
    link: {
        display: 'flex'
    },
    linkBreadcrumb: {
        color: '#c3ebff !important'
    },
    paper: {
        padding: 12,
        color: theme.palette.text.secondary
    },
    eventIcon: {
        fontSize: 32
    },
    event1: {
        display: 'inline-block',
        backgroundColor: '#fb9678',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #fb9678',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #fb9678bd'
    },
    bgEvent1: {
        '& > div': {
            backgroundColor: '#fb9678'
        }
    },
    event2: {
        display: 'inline-block',
        backgroundColor: '#01c0c8',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #01c0c8',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #01c0c8bd'
    },
    bgEvent2: {
        '& > div': {
            backgroundColor: '#01c0c8'
        }
    },
    event3: {
        display: 'inline-block',
        backgroundColor: '#e46a76',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #e46a76',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #e46a76bd'
    },
    bgEvent3: {
        '& > div': {
            backgroundColor: '#e46a76'
        }
    },
    extendedButton: {
        margin: theme.spacing(1),
        padding: '0 16px !important'
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    dangerBtn: {
        backgroundColor: '#dc3f4e',
        '&:hover': {
            backgroundColor: '#cd3342'
        }
    },
    infoKegiatan: {
        display: 'block',
        padding: '24px',
        borderTop: '2px solid #a3ffe8',
        borderBottom: '2px solid #a3ffe8',
        backgroundColor: '#dbfff6',
        borderRadius: '4px',
        boxShadow: '0 10px 10px -15px #dbfff6',
        textAlign: 'center'
    },
    infoTambah: {
        display: 'block',
        padding: '24px',
        borderTop: '2px solid #b6e9ff',
        borderBottom: '2px solid #b6e9ff',
        backgroundColor: '#cdeefd',
        borderRadius: '4px',
        boxShadow: '0 10px 10px -15px #004a6c',
        textAlign: 'center',
        '& p': {
            fontWeight: 700
        }
    }
}));

function BukuKerjaBaru() {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [isFormValid, setIsFormValid] = useState(false);
    const selectedRefBukuKerja = useSelector(({ bukuKerja }) => bukuKerja.bukuKerja.selectedRefPekerjaan);
    const formRef = useRef(null);
    
    function last7Days () {
        return '0123456'.split('').map(function(n) {
            var d = new Date();
            d.setDate(d.getDate() - n);
            
            return (function(day, month, year) {
                return [day<10 ? '0'+day : day, month<10 ? '0'+month : month, year].join('-');
            })(d.getDate(), (d.getMonth()+1), d.getFullYear());
        });
        // .join(',');
    }

    // console.log(last7Days());
    // const data7days = last7Days().map((n, idx) => {
    //     console.log(1);
    //     return "<MenuItem key='" + idx + "' value='" + n + "'>" + n + "</MenuItem>";
    // });
    // console.log(data7days);
//    console.log(tanggalPelaksanaan);
    // function getFullName(item) {
    //     var fullname = [item.firstname, item.lastname].join(" ");
    //     return fullname;
    // }

    function disableButton() {
        setIsFormValid(false);
    }

    function cariKerjaan(textField) {
        console.log(textField);
        // console.log("asasasa");
    }

    function enableButton() {
        setIsFormValid(true);
    }

    function handleSubmitCariKerjaan(model) {
        dispatch(Actions.getRefPekerjaan(model));
    }

    function handleSubmit(model) {
        model.pekerjaanId= selectedRefBukuKerja['@id'];
        dispatch(Actions.buatBukuKerja(model)).then(() => {
            // setSuccess(true);
            dispatch(AppActions.showMessage({ message: 'Data Berhasil dibuat' }));
            History.push('buku-kerja/buku');



        });
        // dispatch(Actions.buatBukuKerja(model));
    }

    return (
        <FusePageSimple
            classes={{
                root        : classes.layoutRoot,
                header      : classNames(classes.bgHeader, "min-h-128 h-128 sm:min-h-52 sm:h-52"),
                content     : "p-12"
            }}
            header={
                <div className="flex flex-1 items-center justify-between p-12">
                    <div>
                        <Typography variant="subtitle1">Cetak Buku Kerja</Typography>
                    </div>
                    <div>
                        <Breadcrumbs separator="›" aria-label="Breadcrumb">
                            <Link to="/" className={classes.linkBreadcrumb}>
                                Beranda
                            </Link>
                            <Link to="/buku-kerja" className={classes.linkBreadcrumb}>
                                Buku Kerja
                            </Link>
                            <Typography color="textPrimary">Baru</Typography>
                        </Breadcrumbs>
                    </div>
                </div>
            }
            content={
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className="w-full text-center p-24">
                                <Typography variant="h5" color="secondary" className="font-bold">PERIODE BUKU KERJA</Typography>
                                <Typography variant="subtitle2" color="textSecondary" gutterBottom>01 Juni 2019 s/d 30 Juni 2019</Typography>
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Formsy
                                onValidSubmit={handleSubmitCariKerjaan}
                                onValid={enableButton}
                                onInvalid={disableButton}
                                ref={formRef}
                                className="flex flex-col justify-center"
                            >
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={12}>
                                        <Typography variant="body1" color="primary" gutterBottom>Masukan Kata Kunci Pekerjaan</Typography>
                                            <TextFieldFormsy
                                                className="w-full mt-12"
                                                type="text"
                                                name="nama"
                                                label="Misal : Rapat, Konsultasi, Apel, dll."
                                                required
                                                variant="outlined"
                                                onkeydown={handleSubmit}
                                            >
                                        </TextFieldFormsy>
                                    </Grid>
                                </Grid>
                                </Formsy>
                                <Formsy
                                    onValidSubmit={handleSubmit}
                                    onValid={enableButton}
                                    onInvalid={disableButton}
                                    ref={formRef}
                                    className="flex flex-col justify-center"
                                >
                                    <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <div className={classes.infoKegiatan}>
                                            <Typography variant="subtitle1" color="primary" className="font-bold">Pilih kegiatan terlebih dahulu! Silakan pilih pada list di bawah ini. </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <BukuKerjaBaruTable />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className={classes.infoTambah}>
                                            <Typography variant="body1" color="primary">Pemberian masukan, koreksi, paraf / tanda tangan oleh atasan atas penyusunan Laporan hasil rapat</Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6} sm={6}>
                                        <Paper className={classes.paper} style={{ textAlign: 'center' }}>
                                            <Typography variant="caption" color="primary">Data Input Pekerjaan :</Typography>
                                            <Typography variant="subtitle1" color="secondary">{selectedRefBukuKerja.tahapanPelaksanaan}</Typography>
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={6} sm={6}>
                                        <Paper className={classes.paper} style={{textAlign: 'center'}}>
                                            <Typography variant="caption" color="primary">Cara Penghitungan :</Typography>
                                            <Typography variant="subtitle1" color="secondary">{selectedRefBukuKerja.keteranganPenghitunganWaktuPoin}</Typography>
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Typography variant="body1" color="primary" gutterBottom>Tanggal Pelaksanaan Kegiatan</Typography>
                                        <SelectFormsy
                                            className="w-full mt-12"
                                            name="tglPelaksanaan"
                                            label="Pilih Tanggal"
                                            value="none"
                                            validationError="Harus diisi!"
                                            variant="outlined"
                                        >
                                            <MenuItem value="none">
                                                <em>Belum Ada</em>
                                            </MenuItem>

                                            {last7Days().map((n, idx) =>
                                                <MenuItem key={idx} value={n}>{n}</MenuItem>
                                            )}
                                        </SelectFormsy>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Typography variant="body1" color="primary" gutterBottom>Kategori Kegiatan</Typography>
                                        <SelectFormsy
                                            className="w-full mt-12"
                                            name="kategori"
                                            label="Pilih Kategori"
                                            value="none"
                                            validationError="Harus diisi!"
                                            variant="outlined"
                                        >
                                            <MenuItem value="none">
                                                <em>Belum Ada</em>
                                            </MenuItem>
                                            <MenuItem value={1}>Tugas Pokok</MenuItem>
                                            <MenuItem value={2}>Tugas Tambahan</MenuItem>
                                        </SelectFormsy>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextFieldFormsy
                                            className="w-full"
                                            type="text"
                                            name="keterangan"
                                            label="Tulis Keterangan Detil Informasi Pekerjaan"
                                            validations={{
                                                minLength: 4
                                            }}
                                            validationErrors={{
                                                minLength: 'Min character length is 4'
                                            }}
                                            required
                                            variant="outlined"
                                            multiline
                                            rows="2"
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextFieldFormsy
                                            className="w-full"
                                            type="number"
                                            name="jumlah"
                                            label="Jumlah Pekerjaan"
                                            required
                                            variant="outlined"
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Button
                                            type="submit"
                                            variant="contained"
                                            color="primary"
                                            className="max-w-200"
                                            aria-label="LOG IN"
                                            disabled={!isFormValid}
                                        >
                                            Ajukan Buku Kerja
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Formsy>
                        </Paper>
                    </Grid>
                </Grid>   
            }
        />
    );
}

export default withReducer('bukuKerja', reducer)(BukuKerjaBaru);