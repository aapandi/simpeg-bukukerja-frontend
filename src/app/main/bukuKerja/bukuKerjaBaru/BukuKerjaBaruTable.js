import React, {useEffect, useState} from 'react';
import {Table, TableBody, TableCell, TableRow} from '@material-ui/core';
import {FuseScrollbars, FuseUtils} from '@fuse';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import BukuKerjaBaruTableHead from './BukuKerjaBaruTableHead';
import * as Actions from '../store/actions';
import {useDispatch, useSelector} from 'react-redux';

function BukuKerjaBaruTable(props) {
    const dispatch = useDispatch();
    const refBukuKerja = useSelector(({ bukuKerja }) => bukuKerja.bukuKerja.dataRefBukuKerja);
    const count = useSelector(({ bukuKerja }) => bukuKerja.bukuKerja.countRef);
    const searchText = useSelector(({bukuKerja}) => bukuKerja.bukuKerja.searchRefText);

    const [selected] = useState([]);
    const [data, setData] = useState(refBukuKerja);
    const [order, setOrder] = useState({
        direction: 'asc',
        id       : null
    });

    // useEffect(() => {
    //     dispatch(Actions.getRefPekerjaan());
    // }, [dispatch]);



    useEffect(() => {
        setData(searchText.length === 0 ? refBukuKerja : FuseUtils.filterArrayByString(refBukuKerja, searchText))
    }, [refBukuKerja, searchText]);

    function handleRequestSort(event, property) {
        const id = property;
        let direction = 'desc';

        if ( order.id === property && order.direction === 'desc' ) {
            direction = 'asc';
        }

        setOrder({
            direction,
            id
        });
    }

    function handleClick(item) {
        dispatch(Actions.setSelectedRefPekerjaan(item));
        // alert('Kamu Sudah Memilih Pekerjaan!');
    }

    return (
        <div className="w-full flex flex-col">

            <FuseScrollbars className="flex-grow overflow-x-auto">

                <Table className="min-w-xl" aria-labelledby="tableTitle">

                    <BukuKerjaBaruTableHead
                        numSelected={selected.length}
                        order={order}
                        onRequestSort={handleRequestSort}
                        rowCount={count}
                    />

                    <TableBody>
                        {
                            _.orderBy(data, [order.direction])
                                .map(n => {
                                    const isSelected = selected.indexOf(n.pekerjaanId) !== -1;
                                    return (
                                        <TableRow
                                            className="h-64 cursor-pointer"
                                            hover
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            key={n.pekerjaanId}
                                            selected={isSelected}
                                            onClick={event => handleClick(n)}
                                        >
                                            <TableCell component="th" scope="row">
                                                {n.nama}
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                {n.satuanHasil}
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                {n.buktiPelaksanaanPekerjaan}
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                {n.keteranganPenghitunganWaktuPoin}
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                {n.tahapanPelaksanaan}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                    </TableBody>
                </Table>
            </FuseScrollbars>
        </div>
    );
}

export default withRouter(BukuKerjaBaruTable);