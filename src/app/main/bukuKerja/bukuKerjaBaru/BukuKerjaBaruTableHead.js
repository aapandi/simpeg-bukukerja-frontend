import React from 'react';
import {TableHead, TableSortLabel, TableCell, TableRow, Tooltip} from '@material-ui/core';

const rows = [
    {
        id              : 'namaPekerjaan',
        align           : 'left',
        disablePadding  : false,
        label           : 'Nama Pekerjaan',
        sort            : true
    },
    {
        id              : 'satuan',
        align           : 'left',
        disablePadding  : false,
        label           : 'Satuan Hasil',
        sort            : true
    },
    {
        id              : 'buktiPekerjaan',
        align           : 'left',
        disablePadding  : false,
        label           : 'Bukti Pekerjaan',
        sort            : true
    },
    {
        id              : 'keterangan',
        align           : 'left',
        disablePadding  : false,
        label           : 'Keterangan Perhitungan',
        sort            : true
    },
    {
        id              : 'metode',
        align           : 'left',
        disablePadding  : false,
        label           : 'Metode Input',
        sort            : true
    }
];

function BukuKerjaBaruTableHead(props) {
    const createSortHandler = property => event => {
        props.onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow className="h-64">
                {rows.map(row => {
                    return (
                        <TableCell
                            key={row.id}
                            align={row.align}
                            padding={row.disablePadding ? 'none' : 'default'}
                            sortDirection={props.order.id === row.id ? props.order.direction : false}
                        >
                            {row.sort && (
                                <Tooltip
                                    title="Sort"
                                    placement={row.align === "right" ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={props.order.id === row.id}
                                        direction={props.order.direction}
                                        onClick={createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            )}
                        </TableCell>
                    );
                }, this)}
            </TableRow>
        </TableHead>
    );
}

export default BukuKerjaBaruTableHead;