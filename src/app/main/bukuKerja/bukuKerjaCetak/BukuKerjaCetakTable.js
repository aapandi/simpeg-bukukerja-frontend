import React, {useEffect, useState} from 'react';
import {makeStyles,Table, TableHead, TableBody, TableCell, TableRow, Typography, } from '@material-ui/core';
import {FuseScrollbars, FuseUtils} from '@fuse';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import * as Actions from '../store/actions';
import {useDispatch, useSelector} from 'react-redux';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
    batalBtn: {
      margin: theme.spacing(1),
      backgroundColor: '#dc3f4e',
      '&:hover': {
        backgroundColor: '#d93343'
      }
    },
    leftIcon: {
      marginRight: theme.spacing(1)
    },
    rightIcon: {
      marginLeft: theme.spacing(1)
    },
    iconSmall: {
      fontSize: 16
    },
    textSuccess: {
        color: '#00c292'
    },
    textPending: {
        color: '#fec107'
    }
}));

function BukuKerjaCetakTable(props) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const bukuKerja = useSelector(({bukuKerja}) => bukuKerja.bukuKerja.data);
    const searchText = useSelector(({bukuKerja}) => bukuKerja.bukuKerja.searchText);

    const [selected] = useState([]);
    const [data, setData] = useState(bukuKerja);
    const [order] = useState({
        direction: 'asc',
        id       : null
    });

    useEffect(() => {
        dispatch(Actions.getBukuKerjas());
    }, [dispatch]);

    useEffect(() => {
        setData(searchText.length === 0 ? bukuKerja : FuseUtils.filterArrayByString(bukuKerja, searchText))
    }, [bukuKerja, searchText]);

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table className="min-w-xl" aria-labelledby="tableTitle">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" width="42">NO.</TableCell>
                            <TableCell align="center">KATEGORI</TableCell>
                            <TableCell>PEKERJAAN</TableCell>
                            <TableCell>DETIL</TableCell>
                            <TableCell align="center">JUMLAH</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {
                            _.orderBy(data, [order.direction])
                                .map((n,key) => {
                                    const isSelected = selected.indexOf(n.id) !== -1;
                                    return (
                                        <TableRow
                                            className="h-64"
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            key={key}
                                            selected={isSelected}
                                        >
                                            <TableCell component="th" scope="row" align="center">
                                                {(key+1)}
                                            </TableCell>
                                            
                                            <TableCell component="th" scope="row" align="center">
                                                {n.kategori}
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                <Typography variant="caption" color="textSecondary">{n.tanggal}</Typography>
                                                <Typography variant="subtitle2" color="primary">{n.namaPekerjaan}</Typography>
                                                {n.status ?
                                                    (
                                                        <Typography className={clsx(classes.textSuccess, "flex justify-start")}>Disetujui</Typography>
                                                    ) :
                                                    (
                                                        <Typography className={clsx(classes.textPending, "flex justify-start")}>Belum Disetujui</Typography>
                                                    )
                                                }
                                            </TableCell>

                                            <TableCell component="th" scope="row">
                                                {n.deskripsi}
                                            </TableCell>

                                            <TableCell component="th" scope="row" align="center">
                                                10
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                    </TableBody>
                </Table>
            </FuseScrollbars>
        </div>
    );
}

export default withRouter(BukuKerjaCetakTable);