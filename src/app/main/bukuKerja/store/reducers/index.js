import {combineReducers} from 'redux';
import bukuKerja from './bukuKerja.reducer';

const reducer = combineReducers({
    bukuKerja
});

export default reducer;
