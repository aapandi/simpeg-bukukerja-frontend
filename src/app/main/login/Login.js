import React, {useEffect, useRef, useState} from 'react';
import {Hidden, Card, CardContent, Typography, Button, InputAdornment, Icon} from '@material-ui/core';
import {FuseAnimate, TextFieldFormsy} from '@fuse';
import Formsy from 'formsy-react';
import {Link} from 'react-router-dom';
import * as authActions from 'app/auth/store/actions';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/styles';
import {useDispatch, useSelector} from 'react-redux';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundImage     : 'url(assets/images/backgrounds/bg-login.jpg)',
        backgroundSize      : 'cover',
        backgroundPosition  : 'right center',
        color               : theme.palette.primary.contrastText
    }
}));

function Login() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const login = useSelector(({auth}) => auth.login);

    const [isFormValid, setIsFormValid] = useState(false);
    const formRef = useRef(null);

    useEffect(() => {
        if ( login.error && (login.error.email || login.error.password) ) {
            formRef.current.updateInputsWithError({
                ...login.error
            });
            disableButton();
        }
    }, [login.error]);

    function disableButton() {
        setIsFormValid(false);
    }

    function enableButton() {
        setIsFormValid(true);
    }

    function handleSubmit(model) {
        dispatch(authActions.submitLogin(model));
    }

    return (
        <div className={clsx(classes.root, "flex flex-col flex-1 flex-shrink-0 p-24 md:flex-row md:p-0 justify-center")}>
            <Hidden mdDown>
            <div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left">
                <FuseAnimate animation="transition.slideUpIn" delay={300}>
                    <Typography variant="h3" color="secondary">Aplikasi Simpeg Buku Kerja</Typography>
                </FuseAnimate>
                <FuseAnimate delay={400}>
                    <Typography variant="h6" color="primary" gutterBottom>Pemerintah Kabupaten Gunungkidul</Typography>
                </FuseAnimate>
                <FuseAnimate delay={500}>
                    <Typography component="p" color="primary" className="max-w-640 mt-16 text-justify">Dalam rangka meningkatkan pelayanan kepegawaian, integrasi data serta mendukung terlaksananya e-Goverment, Badan Kepegawaian, Pendidikan dan Pelatihan Daerah Kabupaten Gunungkidul mengembangkan layanan berbasis elektronik dengan membangun APIK - Aplikasi Pelayanan Informasi Kepegawaian.</Typography>
                </FuseAnimate>
            </div>
            </Hidden>

            <FuseAnimate animation={{translateX: [0, '100%']}}>
                <Card className="w-full max-w-400 mx-auto m-16 md:m-0 " square>
                    <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">
                        <img className="w-92 mb-32" src="assets/images/logos/book.svg" alt="logo"/>

                        <Typography variant="h6" className="text-center md:w-full mb-48">SIMPEG BUKU KERJA</Typography>

                        <div className="w-full">
                            <Formsy
                                onValidSubmit={handleSubmit}
                                onValid={enableButton}
                                onInvalid={disableButton}
                                ref={formRef}
                                className="flex flex-col justify-center w-full"
                            >
                                <TextFieldFormsy
                                    className="mb-16"
                                    type="text"
                                    name="email"
                                    label="Nama Pengguna / Email"
                                    validations={{
                                        minLength: 4
                                    }}
                                    validationErrors={{
                                        minLength: 'Min character length is 4'
                                    }}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end"><Icon className="text-20" color="action">email</Icon></InputAdornment>
                                    }}
                                    variant="outlined"
                                    required
                                />

                                <TextFieldFormsy
                                    className="mb-16"
                                    type="password"
                                    name="password"
                                    label="Kata Sandi"
                                    validations={{
                                        minLength: 4
                                    }}
                                    validationErrors={{
                                        minLength: 'Min character length is 4'
                                    }}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end"><Icon className="text-20" color="action">vpn_key</Icon></InputAdornment>
                                    }}
                                    variant="outlined"
                                    required
                                />

                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    className="w-full mx-auto mt-16 normal-case"
                                    aria-label="MASUK"
                                    disabled={!isFormValid}
                                    value="legacy"
                                >
                                    Masuk
                                </Button>

                            </Formsy>
                        </div>

                        {/* <div className="flex flex-col items-center justify-center pt-32">
                            <Link className="font-medium mt-8" to="/">Kembali ke Beranda</Link>
                        </div> */}
                    </CardContent>
                </Card>
            </FuseAnimate>
        </div>
    )
}

export default Login;
