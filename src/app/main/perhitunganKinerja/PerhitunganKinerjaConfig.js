import React from 'react';
import {authRoles} from "../../auth";

export const PerhitunganKinerjaConfig = {
    settings: {
        layout: {}
    },
    auth:authRoles.user,
    routes  : [
        {
            path     : '/buku-kerja/perhitungan-kinerja',
            component: React.lazy(() => import('./PerhitunganKinerja'))
        }
    ]
};