import React from 'react';
import {Table, TableHead, TableBody, TableCell, TableRow} from '@material-ui/core';
import {FuseScrollbars} from '@fuse';
import {withRouter} from 'react-router-dom';

function PerhitunganKinerjaTable(props) {
    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table className="min-w-xl" aria-labelledby="tableTitle">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" width="42">NO.</TableCell>
                            <TableCell align="center">KATEGORI</TableCell>
                            <TableCell>PEKERJAAN</TableCell>
                            <TableCell>DETIL</TableCell>
                            <TableCell align="center">JUMLAH</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        <TableRow
                            className="h-64"
                            tabIndex={-1}
                        >
                            <TableCell component="th" scope="row" align="center" colSpan="5">
                                TIDAK DITEMUKAN !
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </FuseScrollbars>
        </div>
    );
}

export default withRouter(PerhitunganKinerjaTable);