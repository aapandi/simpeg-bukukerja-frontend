import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Paper, Typography, Breadcrumbs} from '@material-ui/core';
import {Link} from "react-router-dom";
import {FusePageSimple} from '@fuse';
import classNames from 'classnames';
import reducer from './store/reducers';
import withReducer from 'app/store/withReducer';
import VerifikasiBawahanTable from './VerifikasiBawahanTable';

const useStyles = makeStyles(theme => ({
    layoutRoot: {},
    bgHeader: {
        background: '#2c9eff'
    },
    link: {
        display: 'flex'
    },
    linkBreadcrumb: {
        color: '#c3ebff !important'
    },
    paper: {
        padding: 12,
        color: theme.palette.text.secondary
    },
    eventIcon: {
        fontSize: 32
    },
    event1: {
        display: 'inline-block',
        backgroundColor: '#fb9678',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #fb9678',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #fb9678bd'
    },
    bgEvent1: {
        '& > div': {
            backgroundColor: '#fb9678'
        }
    },
    event2: {
        display: 'inline-block',
        backgroundColor: '#01c0c8',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #01c0c8',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #01c0c8bd'
    },
    bgEvent2: {
        '& > div': {
            backgroundColor: '#01c0c8'
        }
    },
    event3: {
        display: 'inline-block',
        backgroundColor: '#e46a76',
        borderRadius: '50%',
        width: 64,
        height: 64,
        lineHeight: '58px',
        textAlign: 'center',
        border: '3px solid #e46a76',
        color: '#fff',
        boxShadow: '0px 5px 10px 0px #e46a76bd'
    },
    bgEvent3: {
        '& > div': {
            backgroundColor: '#e46a76'
        }
    },
    extendedButton: {
        margin: theme.spacing(1),
        padding: '0 16px !important'
    },
    extendedIcon: {
        marginRight: theme.spacing(1)
    },
    dangerBtn: {
        backgroundColor: '#dc3f4e',
        '&:hover': {
            backgroundColor: '#cd3342'
        }
    },
    infoVerifikasi: {
        display: 'block',
        padding: '24px',
        borderTop: '2px solid #b6e9ff',
        borderBottom: '2px solid #b6e9ff',
        backgroundColor: '#cdeefd',
        borderRadius: '4px',
        boxShadow: '0 10px 10px -15px #004a6c',
        textAlign: 'center'
    },
}));

function VerifikasiBawahan() {
    const classes = useStyles();

    return (
        <FusePageSimple
            classes={{
                root        : classes.layoutRoot,
                header      : classNames(classes.bgHeader, "min-h-128 h-128 sm:min-h-52 sm:h-52"),
                content     : "p-12"
            }}
            header={
                <div className="flex flex-1 items-center justify-between p-12">
                    <div>
                        <Typography variant="subtitle1">Verifikasi Bawahan</Typography>
                    </div>
                    <div>
                        <Breadcrumbs separator="›" aria-label="Breadcrumb">
                            <Link to="/" className={classes.linkBreadcrumb}>
                                Beranda
                            </Link>
                            <Typography color="textPrimary">Verifikasi Bawahan</Typography>
                        </Breadcrumbs>
                    </div>
                </div>
            }
            content={
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className="w-full text-center p-24">
                                <Typography variant="h5" color="secondary" className="font-bold">PERIODE BUKU KERJA</Typography>
                                <Typography variant="subtitle2" color="textSecondary" gutterBottom>01 Juni 2019 s/d 30 Juni 2019</Typography>
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <div hidden={true} className={classes.infoVerifikasi}>
                            <Typography variant="h4" color="primary" className="font-bold">Periode verifikasi bulan Mei sudah ditutup</Typography>
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <VerifikasiBawahanTable />
                        </Paper>
                    </Grid>
                </Grid>   
            }
        />
    );
}

// export default (VerifikasiBawahan);
export default withReducer('verifikasiBawahan', reducer)(VerifikasiBawahan);