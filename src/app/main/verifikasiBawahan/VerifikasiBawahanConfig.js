import React from 'react';
import {authRoles} from "../../auth";

export const VerifikasiBawahanConfig = {
    settings: {
        layout: {}
    },
    auth:authRoles.user,
    routes  : [
        {
            path: '/buku-kerja/verifikasi-bawahan',
            component: React.lazy(() => import('./VerifikasiBawahan'))
        }
    ]
};