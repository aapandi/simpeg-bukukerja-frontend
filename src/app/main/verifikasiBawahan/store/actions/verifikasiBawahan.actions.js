import axios from 'axios';

export const GET_ORDERS = '[BUKUKERJA] GET ORDERS';
export const GET_REF_PEKERJAAN = '[BUKUKERJA] GET REF PEKERJAAN';
export const SET_SELECTED_REF_PEKERJAAN = '[BUKUKERJA] SET_SELECTED_REF_PEKERJAAN';
export const SET_BUKU_KERJA_SELECTED = '[BUKUKERJA] SET_BUKU_KERJA_SELECTED';
export const GET_BUKU_KERJAS = '[BUKUKERJA] GET_BUKU_KERJAS';
export const SET_ORDERS_SEARCH_TEXT = '[BUKUKERJA] SET ORDERS SEARCH TEXT';

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export function getVerifikasiBawahans()
{
    const request = axios.get('/api/buku_kerjas');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_BUKU_KERJAS,
                payload: response.data
            })
        );
}

export function getRefPekerjaan(params)
{
    const request = axios.get('/api/pekerjaans',{params});
    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_REF_PEKERJAAN,
                payload: response.data
            })
        );
}

export function buatVerifikasiBawahan(model)
{
    const request = axios.post('/api/buku_kerjas',{
        pekerjaan:model.pekerjaanId,
        keterangan:model.keterangan,
        tglPelaksanaan:model.tglPelaksanaan,
        // tglPelaksanaan: formatDate(Date()),
        kategori:model.kategori,
        statusVerifikasi:1,
        lastUpdateStatus: model.tglPelaksanaan,
        bukuKerjaId: uuidv4()
        // lastUpdateStatus: formatDate(Date())

    });


    return (dispatch) =>
        request.then((response) =>
            dispatch(getVerifikasiBawahans())
        );
}

export function setSelectedRefPekerjaan(params)
{
    return (dispatch) =>
            dispatch({
                type: SET_SELECTED_REF_PEKERJAAN,
                payload: params
            });
}

export function batalkanUsulan(bukuKerjaId)
{
    console.log(bukuKerjaId);
    const request = axios.put('/api/buku_kerjas/'+bukuKerjaId, {
        statusVerifikasi:3
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch(getVerifikasiBawahans())
        );
}
export function setVerifikasiBawahanSelected(bukuKerja)
{
    return (dispatch) =>dispatch({
        type: SET_BUKU_KERJA_SELECTED,
        payload: bukuKerja
    });
}
export function verifikasi(bukuKerjaId)
{
    console.log(bukuKerjaId);
    const request = axios.put('/api/buku_kerjas/'+bukuKerjaId, {
        statusVerifikasi:2
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch(getVerifikasiBawahans())
        );
}

export function perbaikan(bukuKerjaId)
{
    console.log(bukuKerjaId);
    const request = axios.put('/api/buku_kerjas/'+bukuKerjaId, {
        statusVerifikasi:4
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch(getVerifikasiBawahans())
        );
}

export function getOrders()
{
    // const request = axios.get('/api/buku_kerjas');

    // return (dispatch) =>
    //     request.then((response) =>
    //         dispatch({
    //             type   : GET_ORDERS,
    //             payload: response.data
    //         })
    //     );
}

export function filterByBulan()
{
    const request = axios.get('/api/e-commerce-app/orders');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_ORDERS,
                payload: response.data
            })
        );
}

export function setOrdersSearchText(event)
{
    return {
        type      : SET_ORDERS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

