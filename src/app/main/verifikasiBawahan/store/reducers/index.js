import {combineReducers} from 'redux';
import verifikasiBawahan from './verifikasiBawahan.reducer';

const reducer = combineReducers({
    verifikasiBawahan
});

export default reducer;
