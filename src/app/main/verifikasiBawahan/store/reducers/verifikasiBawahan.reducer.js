import * as Actions from '../actions';

const initialState = {
    data      : [],
    dataRefVerifikasiBawahan      : [],
    count: 0,
    countRef: 0,
    selectedRefPekerjaan: [],
    searchText: '',
    searchRefText: '',
    periodeAktif:0,
    verifikasiBawahanSelected: {

    }
};

const verifikasiBawahanReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_BUKU_KERJAS:
        {
            return {
                ...state,
                data: action.payload['hydra:member'],
                count: action.payload['hydra:totalItems']
            };
        }
        case Actions.GET_REF_PEKERJAAN:
        {
            return {
                ...state,
                dataRefBukuKerja: action.payload['hydra:member'],
                countRef: action.payload['hydra:totalItems']
            };
        }
        case Actions.SET_SELECTED_REF_PEKERJAAN:
        {
            console.log(action.payload);
            return {
                ...state,
                selectedRefPekerjaan: action.payload
            };
        }
        case Actions.GET_ORDERS:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_ORDERS_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        case Actions.SET_BUKU_KERJA_SELECTED:
        {
            return {
                ...state,
                verifikasiBawahanSelected: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default verifikasiBawahanReducer;
